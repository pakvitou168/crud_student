<?php

use Illuminate\Database\Seeder;
// use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 
        // $data = [
        // 	[
        //     'name' => 'vitou',
        //     'email' => 'pakvitou'.'@gmail.com',
        //     'password' => bcrypt('123456'),
        // ],
        // [
        //     'name' => 'vitou',
        //     'email' => 'pakvitou'.'@gmail.com',
        //     'password' => bcrypt('123456'),
        // ]
        // ];
        // DB::table('users')->insert($data);

        DB::table('users')->insert([
            'name' => 'demo',
            'email' => 'pakvitou1'.'@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        
    }
    //  public function index()
    // {
    //     // 
    //     DB::table('users')->insert([
    //         'name' => 'vitou',
    //         'email' => 'pakvitou'.'@gmail.com',
    //         'password' => bcrypt('123456'),
    //     ]);
    // }
}
