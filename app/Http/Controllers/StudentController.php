<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use function Sodium\compare;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
//        $a =Student::where('user_id', \auth()->id())->get();;
//        Return($a);
        $students = new Student;
//        $students = Student::all();
        if ($request->firstname) {

            $students = $students
                ->where('firstname', 'like', '%' . $request->firstname . '%');
            $students = $students->paginate(3);
            if (empty($students)) {
                $students = [

                ];
            }
        } else {
            $students = $students::where('user_id',auth()->id())->paginate(3);
//            return ($students);
        }
        return view('CRUD/index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('CRUD/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required|min:3|max:255',
            'lastname' => 'required|min:3|max:255'
        ]);
        $students = new Student();
        $students->user_id = auth()->id();
        $students->firstname = $request->firstname;
        $students->lastname = $request->lastname;
        $students->save();
        Session::flash('message', 'Add Success !' . $students->firstname . $students->lastname);

        return redirect('/student');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\student $student
     * @return \Illuminate\Http\Response
     */
    public function show(student $student, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\student $student
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $student = Student::find($id);
        // return ($students);
        return view('CRUD/update', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\student $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $student = Student::find($id);
        $student->firstname = $request->firstname;
        $student->lastname = $request->lastname;
        $student->save();
        Session::flash('message', 'Update success!');
        return redirect('/student');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\student $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $student = Student::find($id);
        $student->delete();
        Session::flash('message', 'Delete success!');
        return redirect('/student');
    }

    public function search(Request $request)
    {

        $firstName = $request->firstname;
        $students = Student::where('firstname', 'like', '%' . $firstName . '%')->orderBy('firstname')->paginate('3');
//       if (count($students) !== 0) {
        return view('/CRUD/index', compact('students'));
//        } else {
////            Session::flash('message', 'Empy !');
//            $noQuery = "Name '" . $firstName . "' does not exist!";
//            return redirect('student', compact('noQuery'));
//        }

    }

//    @@sortby id
    public function sortFstudent()
    {
//        $students = Student::orderBy('id', 'asc')->paginate(3);

        return view('/CRUD/index', compact('students'));
    }

    public function sortLstudent()
    {
        $students = Student::orderBy('id', 'desc')->paginate(3);
        return view('/CRUD/index', compact('students'));
    }

    public function checkDelete(Request $request_id)
    {
//        return($request_id);

        $req = $request_id->input('checkbox');
        if (empty($req)) {
            Session::flash('message', 'Check atleast one to delete!');
            return redirect('student');
        } else {
            foreach ($req as $sID) {
                Student::where('id', $sID)->delete();
            }
            Session::flash('message', 'Delete success!');

            return redirect('student');
        }
    }

    function lStudent()
    {
        $students = Student::where('user_id', auth()->id())->paginate(3);
//        return($students);

        return view('CRUD.list', compact('students'));
    }


}
