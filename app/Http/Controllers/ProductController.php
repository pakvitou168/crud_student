<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductFormValidation;
use App\Http\Requests\StoreFormValidation;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\SearchFormValidation;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Session;
// use Illuminate\Http\Request;
use App\Repositories\User\UserRepository;
class ProductController extends Controller
{
    protected $productRepo;

   public function __construct(UserRepository $productRepo)
   {
       $this->productRepo = $productRepo;
   }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
           
        $products = $this->productRepo;


        if ($request->productName) {
            $products = $products
                ->where('productName', 'like', '%' . $request->productName .'%');
            $products = $products->paginate(3);
            if (empty($products)) {
                $products = [

                ];
            }
        } else {

            $products = $products->getAll();
       
        }
        return view('Product.index', compact('products'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('Product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFormValidation $request)
    {
//        $request->validate([
//            'firstname' => 'required|min:3|max:255',
//            'lastname' => 'required|min:3|max:255'
//        ]);
        $products = new Product();
        $products->user_id = auth()->id();
        $products->productName = $request->productName;
        $products->price = $request->price;
        $products->save();
          Session::flash('message', 'Add Success !');
//        dd($products->save());
        return redirect('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
//        $users = User::all();
//        dd($product);
        return view('Product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
//        dd($id);
        $products = Product::find($id);
//        dd($products);
        $products->productName = $request->productName;
        $products->price = $request->price;
        $products->save();
          Session::flash('message', 'Update Success !');
        return redirect('product');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $product)
    {
        //
    }

}


//        