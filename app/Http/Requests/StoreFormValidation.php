<?php

namespace App\Http\Requests;

use App\Product;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'productName' => 'required|max:255|unique:products',
            'price' => 'required'
        ];
    }

    // public function messages()
    // {
    //     return [
    //         'productName.required' => 'Production name is required',
    //         'productName.max' => 'Production name is maximum 255',
    //         'price.required' => 'price is also required',
    //         'productName.unique' => 'product name  is also token',
    //     ];
    // }
}
