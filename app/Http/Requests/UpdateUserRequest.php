<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
//use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     *
     *
     */

    //----------Check if product name is duplicated to other product but ignore on itself.------------------//
    public function rules(Request $request)
    {
        return [
            'productName' => 'required|unique:products,productName,'.$request->id,
        ];
    }

    public function messages()
    {


        return [
            'productName.required' => 'Production name is required'
        ];
    }
}
