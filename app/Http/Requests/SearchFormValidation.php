<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
// use App\Http\Requests\Validator;
use Validator;
class SearchFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
           $validator = Validator::make($request->all(), [
            'productName' => 'required'
            ]);

        if ($validator->fails()) {
            return redirect('Product/index')
                        ->withErrors($validator)
                        ->withInput();
        }

    }
    // public function messages(){
    //     return[
    //         'productName.required'=>'productName is required'
    //     ];
        
    // }
}
