<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         $models = array(
            'Task',
            'User ',
        );

        foreach ($models as $model) {
            $this->app->bind("App\Repositories\\$model\\{$model}Repository", "App\Repositories\\$model\\Eloquent{$model}");

        }
    }
}
