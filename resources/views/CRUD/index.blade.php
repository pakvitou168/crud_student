@extends('layouts.app')
@section('title','Add student')
@section('head')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        body {

            font-family: 'Nunito Semibold';
            text-align: center;
            background-color: #fff;
        }

        .content {

            max-width: 1080px;
            margin: 0 auto;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .button2 {
            background-color: #008CBA;
        }

        /* Blue */
        .button3 {
            background-color: #f44336;
        }

        /* Red */
        .sb {
            color: #000;
        }
    </style>
@endsection
@section('content')
    <div class="content">
        <form action="{{route('student')}}" method="get">
            @csrf
            Enter first name:
            <input type="text" name="firstname" placeholder="Query..." required="required">
            <input type="submit" value="Search"> | <a href="{{route('lStudent')}}"
                                                      class="button button2">list
                all</a>
        </form>
        <form action="{{route('checkDelete')}}" method="get">
            {{csrf_field()}}
            <table style="text-align: center">
                <tr>
                    <th><a href="{{route('student/create')}}" class="button">Add Student</a></th>
                    <th colspan="4">
                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        @endif
                    </th>
                </tr>
                <tr>
                    <th><input type="checkbox" class="check" id="checkAll">&nbsp;Check all</th>
                    <th>Student ID</th>
                    <th><a href="{{route('fstudent')}}" class="sb"> Firstname</a></th>
                    <th><a href="{{route('lstudent')}}" class="sb"> Lastname </a></th>
                    <th>option</th>
                </tr>
                @if($students->count())
                    @foreach( $students as $student)
                        <tr>
                            <th><input type="checkbox" class="check" name="checkbox[]" value="{{$student->id}}"></th>
                            <th>{{$student->id}}</th>
                            <td>{{$student->firstname}}</td>
                            <td>{{$student->lastname}}</td>
                            <td>
                                <a href="student/{{$student->id}}/edit" class="button button2">Edit</a>|<a
                                        href="student/{{$student->id}}/destroy"
                                        onclick="return confirm('Are you sure you want to delete this item?');"
                                        class="button button3">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="4">{{$students->links()}}</td>
                        <td><input type="submit" class="button button3" value="Checked Delete"></td>
                    </tr>
                @else
                    <tr>
                        <td colspan="4" style="color: red;">no q</td>
                    </tr>
                @endif
            </table>
        </form>

    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $("#checkAll").click(function () {
            $(".check").prop('checked', $(this).prop('checked'));
        });
    </script>

@endsection