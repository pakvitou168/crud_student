@extends('layouts.app')

@section('head')
    <style type="text/css">
        body {

            font-family: 'Nunito Semibold';
            text-align: center;
            background-color: #fff;
            font-size: 22px;

        }

        #content {

            max-width: 960px;
            margin: 0 auto;
            overflow: hidden;
        }

        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        #container {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }
    </style>
@endsection

@section('content')

    <div id="content">
        <div id="container">
            <form action="{{route('update',$student->id)}}" method="post">
                @csrf
                <p>FirstName</p>
                <textarea name="firstname" rows="1" cols="50" placeholder="First name" style="overflow-y: hidden;">{{$student->firstname}}
			</textarea>
                <p>LastName</p>
                <textarea name="lastname" rows="1" cols="50" placeholder="First name" style="overflow-y: hidden;">{{$student->lastname}}
			</textarea>
                <br>
                <input type="submit" value="Save">
            </form>
        </div>
    </div>




@endsection