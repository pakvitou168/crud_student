@extends('layouts.app')

@section('head')
    <style type="text/css">
        body {

            font-family: 'Nunito Semibold';
            text-align: center;
            background-color: #fff;
        }

        #content {

            max-width: 960px;
            margin: 0 auto;
        }

        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        #container {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }
    </style>
@endsection

@section('content')
    <div id="content">
        <div id="container">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('student/store')}}" method="post">
                @csrf
                <label for="fname">First Name</label>
                <input type="text" id="fname" name="firstname" placeholder="Your first name..">

                <label for="lname">Last Name</label>

                <input type="text" id="lname" name="lastname" placeholder="Your last name..">
                <input type="submit" value="Submit">
            </form>
        </div>
    </div>
@endsection