@extends('layouts.app')
@section('title','_product')
@section('head')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        body {

            font-family: 'Nunito Semibold';
            text-align: center;
            background-color: #fff;
        }

        .content {

            max-width: 930px;
            margin: 0 auto;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .button2 {
            background-color: #008CBA;
        }

        /* Blue */
        .button3 {
            background-color: #f44336;
        }

        /* Red */
        .sb {
            color: #000;
        }
    </style>


@endsection
@section('content')

    <div class="content">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{ Form::open(array('url' => 'product/store')) }}
        <div class="container-fluid"><!-- Row 1 -->
            <div class="col-lg-4" id="userFormColumn2">
                <div class="form-group required">
                    {!! Form::label('pname', 'Product Name: ', ['class' => 'control-label']) !!}
                    {!! Form::text('productName', '',['class' => 'form-control input-normal','placeholder' => 'Product Name']) !!}
                </div>
            </div>

            <div class="col-lg-4" id="userFormColumn2">
                <div class="form-group required ">
                    {!! Form::label('price', 'Price: ', ['class' => 'control-label']) !!}
                    {!! Form::text('price','', ['class' => 'form-control input-normal','placeholder' => 'Price']) !!}
                </div>
            </div>
            <div class="col-lg-4" id="userFormColumn2">
                <div class="form-group required ">
                    {!! Form::submit('Add', ['class' => 'btn btn-success btn-sm fa fa-trash']) !!}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>




@endsection
