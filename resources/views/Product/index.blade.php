@extends('layouts.app')
@section('title','_product')
@section('head')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        body {

            font-family: 'Nunito Semibold';
            /*text-align: center;*/
            background-color: #fff;
        }

        .content {

            max-width: 1080px;
            margin: 0 auto;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .button2 {
            background-color: #008CBA;
        }

        /* Blue */
        .button3 {
            background-color: #f44336;
        }

        /* Red */
        .sb {
            color: #000;
        }
		.dfsfsafasfsad
    </style>


@endsection
@section('content')
       <!--  <form action="{{route('product')}}" method="get">
            @csrf
            <input type="text" name="productName" placeholder="Query..." required="required">
            <input type="submit" value="Search">
        </form> -->
  
    <div class="content">
        <div class="container">
           <div class="row">
                <div class="col-sm-3">
                  <a href="{{route('product.create')}}"><b>Add Product</b></a>
                </div>
                <div class="col-sm-6">
                 {{Form::open(array('route' => array('product'),'method'=>'get'))}}
                    <!-- {!! Form::label('pname', 'Product Name: ', ['class' => 'control-label']) !!} -->
                    {!! Form::text('productName','',['class' => 'form-control input-normal','placeholder' =>'product Name']) !!}
             
                    {!! Form::submit('Search', ['class' => 'btn btn-success btn-sm fa fa-trash']) !!}
           
                {{ Form::close() }}
                </div>
           </div>
        </div>
        <br>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Product Name</th>
                <th scope="col">Price</th>
                <th scope="col">Create_at</th>
                <th scope="col">Option</th>
            </tr>
            </thead>
            @if($products->count())
                @foreach( $products as $product)
                    <tbody>
                    <tr>
                        <th scope="row">{{$product->id}}</th>
                        <td>{{$product->productName}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->created_at}}</td>
                        <td> <a href="{{route('product.edit',$product->id)}}"><b>Edit Product</b></a></td>
                    </tr>
                    
                @endforeach
            @else
                <tr><td style="color: red;font-size: 24;font-weight: bold;">No query</td></tr>
            @endif
            </tbody>

        </table>
    </div>



@endsection
