<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // echo phpinfo();
    return view('welcome');
});
Route::get('/test', 'TestController@index')->name('test');
Route::any('/student', 'StudentController@index')->name('student');
Route::any('/student/create', 'StudentController@create')->name('student/create');
Route::post('/student/store', 'StudentController@store')->name('student/store');
Route::any('/student/{id}/edit', 'StudentController@edit')->name('edit');
Route::any('/student/{id}/update', 'StudentController@update')->name('update');
Route::any('/student/{id}/destroy', 'StudentController@destroy')->name('destroy');
Route::any('/search', 'StudentController@search')->name('search');
Route::any('/firstStudent', 'StudentController@sortFstudent')->name('fstudent');
Route::any('/LastStudent', 'StudentController@sortLstudent')->name('lstudent');
Route::any('/checkDelete', 'StudentController@checkDelete')->name('checkDelete');
Route::any('/lStudent', 'StudentController@lStudent')->name('lStudent');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('product','ProductController@index')->name('product');
Route::any('product/create','ProductController@create')->name('product.create');
Route::any('product/store','ProductController@store')->name('product.store');
Route::any('product/{id}/edit','ProductController@edit')->name('product.edit');
Route::post('product/update/{id}/','ProductController@update')->name('product.update');

 
// Task Routes
Route::group(['prefix' => 'tasks'], function () {
	Route::get('/{id?}', [
	    'uses' => 'TaskController@getAllTasks',
	    'as' => 'task.index'
	]);
 
	Route::post('store', [
	    'uses' => 'TaskController@postStoreTask',
	    'as' => 'task.store'
	]);
 
	Route::patch('{id}/update', [
	    'uses' => 'TaskController@postUpdateTask',
	    'as' => 'task.update'
	]);
 
	Route::delete('{id}/delete', [
	    'uses' => 'TaskController@postDeleteTask',
	    'as' => 'task.delete'
	]);
});